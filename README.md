Utility that takes configuration from ispconfig_mysql.yaml to obtain the list of active databases in ispconfigs, then
merging with credentials from mysql_servers.yaml dumps to stdout in yaml format. 

It also takes configuration from ispconfig_mysql.yaml to obtain list of active web sites directories in ispconfigs, then
merging with properties from web_servers.yaml dumps to stsdout in yaml format.

If borgmatic base configuration is specified then it can merge the configurations. 

Examples of ymls are provided.

```
usage: ispconfig_dbs_borgmatic [-h] [-m] [-b BORGM] [-i ISP]
                               [-s MYSQL_SERVERS] [-w WEB_SERVERS] [-x] [-d]
                               [-f]

optional arguments:
  -h, --help            show this help message and exit
  -m, --merge           merge with existing borgmatic configuration specified
                        in -b argument (default: False)
  -b BORGM, --borgm BORGM
                        the borgmatic base configuration path where new
                        databases will be merged (default:
                        /etc/borgmatic.d/mysql.yaml)
  -i ISP, --isp ISP     ispconfig database configuration path (default:
                        /etc/borgmatic/ispconfig_mysql.yaml)
  -s MYSQL_SERVERS, --mysql_servers MYSQL_SERVERS
                        mysql servers yaml configuration path (default:
                        /etc/borgmatic/mysql_servers.yaml)
  -w WEB_SERVERS, --web_servers WEB_SERVERS
                        web servers yaml configuration path (default:
                        /etc/borgmatic/web_servers.yaml)
  -x, --lxc             if it's there the lxc rootfs will be prefixed in all
                        paths of the web_servers it makes sense to be used in
                        conjunction with -w. (default: False)
  -d, --dbs             if it's there it will try to build a list of mysql
                        databases (default: False)
  -f, --files           if a list of website directories must be built.
                        (default: False)
  -e EXCLUDE, --exclude EXCLUDE
                        exclude the sites/files/databases whose the domain match with this regexp. (default: None)
                                                
```

# Installation

install python3-mysql.connector

for tags:

`pip3 install https://gitlab.com/communia/ispconfig_dbs_borgmatic/-/archive/v0.1/ispconfig_dbs_borgmatic-v0.1.zip`

or master:

`pip3 install https://gitlab.com/communia/ispconfig_dbs_borgmatic/-/archive/master/ispconfig_dbs_borgmatic-master.zip`

